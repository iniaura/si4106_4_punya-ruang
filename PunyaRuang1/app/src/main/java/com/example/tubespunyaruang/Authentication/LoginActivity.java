package com.example.tubespunyaruang.Authentication;

import android.content.ContentProvider;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.tubespunyaruang.Homepage;
import com.example.tubespunyaruang.MainActivity;
import com.example.tubespunyaruang.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.google.android.gms.auth.api.signin.GoogleSignIn.getSignedInAccountFromIntent;

public class LoginActivity extends AppCompatActivity {
    ConstraintLayout afterLogin;
    LinearLayout beforeLogin;
    static final String TAG = "cobadlu";
    static final int GOOGLE_SIGN = 123;
    static final int GOOGLE_SIGN_OUT = 007;
    FirebaseAuth mAuth;
    Button btn_login, btn_google;
    EditText textemail, textpassword;
    TextView textnama;
    ProgressBar progressBar;
    GoogleSignInClient mGoogleSignInClient;
    private GoogleApiClient mGoogleApiClient;
    public View view;
    private FirebaseAuth firebaseAuth;
    DatabaseReference reff;
    FirebaseDatabase fbdb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btn_login = findViewById(R.id.btn_login);
        btn_google = findViewById(R.id.login);
        textnama = findViewById(R.id.textnama);
        textemail = findViewById(R.id.email);
        textpassword = findViewById(R.id.password);
        progressBar = findViewById(R.id.progress_circular);
        afterLogin= findViewById(R.id.afterlogin);
        beforeLogin= findViewById(R.id.layoutUtama);
        mAuth = FirebaseAuth.getInstance();
        beforeLogin.setVisibility(View.VISIBLE);
        afterLogin.setVisibility(View.GONE);
        firebaseAuth = FirebaseAuth.getInstance();
        textpassword =  findViewById(R.id.password);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(signInIntent, GOOGLE_SIGN);
                    }
                });
                //Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                //Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogl.eApiClient);

               // startActivityForResult(signInIntent, GOOGLE_SIGN);
                //Log.i(TAG, "SignInGoogle: "+mGoogleSignInClient.getInstanceId());
            }
        });


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = textemail.getText().toString().trim();
                String password = textpassword.getText().toString().trim();
                Log.i(TAG, "onClick: ");

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(LoginActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(LoginActivity.this, "Password too short", Toast.LENGTH_SHORT).show();
                }

                progressBar.setVisibility(View.VISIBLE);

                    firebaseAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {

                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    progressBar.setVisibility(View.GONE);

                                    if (task.isSuccessful()) {

                                        //FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                        //reff = FirebaseDatabase.getInstance().getReference().child("User").getParent();
                                        Intent intent = new Intent(LoginActivity.this,Homepage.class);
                                        //intent.putExtra("namadisplay", String.valueOf(FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())));
                                        //intent.putExtra("emaildisplay",String.valueOf(FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getEmail())));
                                        //Log.i(TAG, "onComplete: "+reff);
                                        intent.putExtra("namadisplay",mAuth.getCurrentUser().getDisplayName());
                                        intent.putExtra("emaildisplay",mAuth.getCurrentUser().getEmail());
                                        Log.i(TAG, "onComplete: "+mAuth.getCurrentUser().getDisplayName());
                                        startActivity(intent);
//                                        startActivity(new Intent(getApplicationContext(), Homepage.class));
                                        Toast.makeText(LoginActivity.this, "Login berhasil", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Login Gagal", Toast.LENGTH_SHORT).show();


                                    }

                                }
                            });




            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN) ;
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        //Task<GoogleSignInAccount> task = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        handleSignResult(task);

    }

    private void handleSignResult(Task<GoogleSignInAccount> completedTask){
        try {
            GoogleSignInAccount acc = completedTask.getResult(ApiException.class);
            Toast.makeText(this, "Login with google success", Toast.LENGTH_SHORT).show();
            firebaseAuthWithGoogle(acc);

        }catch (ApiException e){
            Toast.makeText(this, "Failed Login with google", Toast.LENGTH_SHORT).show();
            firebaseAuthWithGoogle(null);
        }
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.i(TAG, "firebaseAuthWithGoogle:" + account.getId());

        AuthCredential credential = GoogleAuthProvider
                .getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()){
                            Intent intent = new Intent(LoginActivity.this,Homepage.class);
                            intent.putExtra("namadisplay",mAuth.getCurrentUser().getDisplayName());
                            intent.putExtra("emaildisplay",mAuth.getCurrentUser().getEmail());
                            intent.putExtra("inilogingoogle",10);
                            Log.i(TAG, "onComplete: "+mAuth.getCurrentUser().getDisplayName());
                            startActivity(intent);

                       Toast.makeText(LoginActivity.this, "Login with google success", Toast.LENGTH_SHORT).show();

                   }else{
                       Toast.makeText(LoginActivity.this, "Failed Login with google", Toast.LENGTH_SHORT).show();

                   }
                    }
                });

    }

    public void back(View view) {onBackPressed();
    }

    public void gotoRegister(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }
}
