package com.example.tubespunyaruang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tubespunyaruang.Authentication.LoginActivity;
import com.example.tubespunyaruang.Authentication.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Profil extends AppCompatActivity {
    Button SignOut,editProfile;
    Intent intent;
    String nama,email;
    int kodegoogle;
    TextView namaProfile,emailProfile;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        SignOut = findViewById(R.id.btn_sign_out);
        namaProfile = findViewById(R.id.namaProfile);
        emailProfile = findViewById(R.id.emailProfile);
        editProfile = findViewById(R.id.btn_edit_profile);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase =FirebaseDatabase.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
//
//        DatabaseReference databaseReference = firebaseDatabase.getReference(firebaseAuth.getUid());
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                User user = dataSnapshot.getValue(User.class);
//                namaProfile.setText(user.getUsername());
//                emailProfile.setText(user.getEmail());
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Toast.makeText(Profil.this, "database error"+databaseError.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });


        intent = getIntent();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            nama = bundle.getString("namadisplay");
            email = bundle.getString("emaildisplay");
            kodegoogle = bundle.getInt("inilogingoogle");

            //kodegoogle = Integer.parseInt(bundle.getString("inilogingoogle"));
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            namaProfile.setText(firebaseUser.getDisplayName());
            emailProfile.setText(firebaseUser.getEmail());
        }



        SignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(Profil.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profil.this,EditProfilActivity.class);
                intent.putExtra("inilogingoogle",kodegoogle);
                startActivity(intent);
            }
        });
    }

    public void back(View view) {
        onBackPressed();
    }


    public void editprofil(View view) {
    }
}
