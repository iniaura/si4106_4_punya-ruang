package com.example.tubespunyaruang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HistoryOrder extends AppCompatActivity {

    TextView nama_ruang1, acacaHO, penyelenggaraHO, dateHO, timeHO;
    private DatabaseReference database;

    private ArrayList<pesanan> daftarPesanan;
    private PesananAdapterRecyclerView pesananAdapterRecyclerView;

    private RecyclerView rv_list_pesanan;
    private ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_order);

        nama_ruang1 = findViewById(R.id.nama_ruang1);
        acacaHO = findViewById(R.id.acacaHO);
        penyelenggaraHO = findViewById(R.id.penyelenggaraHO);
        dateHO = findViewById(R.id.dateHO);
        timeHO = findViewById(R.id.timeHO);

        database = FirebaseDatabase.getInstance().getReference();
//        rv_list_pesanan = findViewById(R.id.rv_list_pesanan);
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        rv_list_pesanan.setLayoutManager(mLayoutManager);
//        rv_list_pesanan.setItemAnimator(new DefaultItemAnimator());

//        loading = ProgressDialog.show(HistoryOrder.this,
//                null,
//                "Please wait...",
//                true,
//                false);

//        database.child("pesanan").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                daftarPesanan = new ArrayList<>();
//                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){

//                    pesanan pesanan = noteDataSnapshot.getValue(pesanan.class);
//                    pesanan.setKey(noteDataSnapshot.getKey());

//                    daftarPesanan.add(pesanan);

//                }
//                pesananAdapterRecyclerView = new PesananAdapterRecyclerView(daftarPesanan);
//                rv_list_pesanan.setAdapter(pesananAdapterRecyclerView);
//                loading.dismiss();
//            }

//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
//                loading.dismiss();
//            }
//        });

    }

    public void card_infohistory(View view) {
        String ruangan = nama_ruang1.getText().toString();
        String acara = acacaHO.getText().toString();
        String penyelenggara = penyelenggaraHO.getText().toString();
        String date = dateHO.getText().toString();
        String time = timeHO.getText().toString();

        Intent intent = new Intent(HistoryOrder.this, HistoryDetailActivity.class);
        intent.putExtra("namaRuangHO", ruangan);
        intent.putExtra("acaraHO", acara);
        intent.putExtra("penyelenggaraHO", penyelenggara);
        intent.putExtra("dateHO", date);
        intent.putExtra("timeHO", time);
        startActivity(intent);
    }
}
