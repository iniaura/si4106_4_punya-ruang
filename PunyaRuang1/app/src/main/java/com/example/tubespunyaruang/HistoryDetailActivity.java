package com.example.tubespunyaruang;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HistoryDetailActivity extends AppCompatActivity {

    String ruanganHO, acaraHO, penyelenggaraHO, dateHO, timeHO;
    TextView tv_nama_ruangHO, output_acaraHO, output_penyelenggaraHO, output_dateResultHO, output_timeResultHO;
    Button btn_batal, btn_reschedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_detail);

        ruanganHO = getIntent().getExtras().getString("namaRuangHO");
        acaraHO = getIntent().getExtras().getString("acaraHO");
        penyelenggaraHO = getIntent().getExtras().getString("penyelenggaraHO");
        dateHO = getIntent().getExtras().getString("dateHO");
        timeHO = getIntent().getExtras().getString("timeHO");

        tv_nama_ruangHO = findViewById(R.id.tv_nama_ruangHO);
        output_acaraHO = findViewById(R.id.output_acaraHO);
        output_penyelenggaraHO = findViewById(R.id.output_penyelenggaraHO);
        output_dateResultHO = findViewById(R.id.output_dateResultHO);
        output_timeResultHO = findViewById(R.id.output_timeResultHO);

        tv_nama_ruangHO.setText(ruanganHO);
        output_acaraHO.setText(acaraHO);
        output_penyelenggaraHO.setText(penyelenggaraHO);
        output_dateResultHO.setText(dateHO);
        output_timeResultHO.setText(timeHO);

        btn_batal = findViewById(R.id.btn_batal);
        btn_batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HistoryDetailActivity.this, Pembatalan.class));
            }
        });

        btn_reschedule = findViewById(R.id.btn_reschedule);
        btn_reschedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HistoryDetailActivity.this, Reschedule.class));
            }
        });

    }

    public void clickBack(View view) {
        onBackPressed();
    }
}
