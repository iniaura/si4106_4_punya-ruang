package com.example.tubespunyaruang.Authentication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.strictmode.IntentReceiverLeakedViolation;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.transition.Visibility;

import com.example.tubespunyaruang.Homepage;
import com.example.tubespunyaruang.MainActivity;
import com.example.tubespunyaruang.R;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "cobadulu";
    private EditText email1;
    private EditText Password1;
    private EditText confirmpassword;
    Button btn_regis;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    private EditText nama;
    DatabaseReference databaseReference;
    FirebaseDatabase firebaseDatabase;


        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);

            progressDialog = new ProgressDialog(this);
            btn_regis =  findViewById(R.id.btn_regis);
            email1= findViewById(R.id.email);
            Password1 =  findViewById(R.id.password);
            confirmpassword= findViewById(R.id.confirmpassword);
            progressBar = findViewById(R.id.ProgressBar);
            nama = findViewById(R.id.regisnama);
            databaseReference = FirebaseDatabase.getInstance().getReference("User");
            firebaseAuth = FirebaseAuth.getInstance();


            GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();


            btn_regis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String email = email1.getText().toString().trim();
                    String regisnama = nama.getText().toString().trim();
                    String password = Password1.getText().toString().trim();
                    String confirpassword = confirmpassword.getText().toString().trim();

                    if (TextUtils.isEmpty(email)) {
                        Toast.makeText(RegisterActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(regisnama)) {
                        Toast.makeText(RegisterActivity.this, "Please Enter your Name", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(password)) {
                        Toast.makeText(RegisterActivity.this, "please enter password", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (TextUtils.isEmpty(confirpassword)) {
                        Toast.makeText(RegisterActivity.this, "please enter confirm password", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (password.length() < 6) {
                        Toast.makeText(RegisterActivity.this, "Password too short", Toast.LENGTH_SHORT).show();
                    }

                    progressBar.setVisibility(View.VISIBLE);

                    if (password.equals(confirpassword)) {

                        firebaseAuth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {

                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        progressBar.setVisibility(View.GONE);
                                        if (task.isSuccessful()) {

                                            User information = new User(
                                                    regisnama,
                                                    password,
                                                    email
                                                    );

                                            FirebaseDatabase.getInstance().getReference("User")
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                    .setValue(information).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    startActivity(new Intent(getApplicationContext(), Homepage.class));
                                                    Toast.makeText(RegisterActivity.this, "Registrasi berhasil", Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                        } else {
                                            Toast.makeText(RegisterActivity.this, "Registration failed", Toast.LENGTH_SHORT).show();


                                        }

                                    }
                                });


                    }

                    }
                });

        }
}