package com.example.tubespunyaruang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tubespunyaruang.Authentication.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;


public class pemesanan extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener,
        View.OnClickListener,
        TimePickerDialog.OnTimeSetListener {

    Intent getIntent;
    EditText input_acara, input_penyelenggara;
    TextView tv_dateResult, tv_timeResult;
    Button btn_datePicker, btn_timePicker;
    Button btn_ok;
    String namaRuang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan);

        input_acara = findViewById(R.id.input_acara);
        input_penyelenggara = findViewById(R.id.input_penyelenggara);
        tv_dateResult = findViewById(R.id.tv_dateResult);
        tv_timeResult = findViewById(R.id.tv_timeResult);

        btn_datePicker = findViewById(R.id.btn_datePicker);
        btn_datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.btn_datePicker:
                        DatePickerFragment datePickerFragment = new DatePickerFragment();
                        datePickerFragment.show(getSupportFragmentManager(), "Date Picker");
                        break;
                }
            }
        });

        btn_timePicker = findViewById(R.id.btn_timePicker);
        btn_timePicker.setOnClickListener(this);

//        get namaruang
        getIntent = getIntent();
        Bundle bundle = getIntent().getExtras();
        namaRuang = bundle.getString("namaRuang");


        btn_ok = findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String acara = input_acara.getText().toString();
                String penyelenggara = input_penyelenggara.getText().toString();
                String date = tv_dateResult.getText().toString();
                String time = tv_timeResult.getText().toString();

                if(TextUtils.isEmpty(acara)){
                    Toast.makeText(pemesanan.this, "Nama acara belum diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(penyelenggara)){
                    Toast.makeText(pemesanan.this, "Nama penyelenggara belum diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(date)){
                    Toast.makeText(pemesanan.this, "Tanggal belum dipilih", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(time)){
                    Toast.makeText(pemesanan.this, "Jam belum dipilih", Toast.LENGTH_SHORT).show();
                    return;
                }

                pesanan pesanan = new pesanan(
                        namaRuang,
                        acara,
                        penyelenggara,
                        date,
                        time
                );

                // Write a message to the database
                FirebaseDatabase.getInstance().getReference("pesanan")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(pesanan).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(pemesanan.this,"berhasil dipesan", Toast.LENGTH_SHORT ).show();
                        Intent intent = new Intent(getApplicationContext(), resultPemesanan.class);
                        intent.putExtra("namaRuang", namaRuang);
                        intent.putExtra("acara", acara);
                        intent.putExtra("penyelenggara", penyelenggara);
                        intent.putExtra("date", date);
                        intent.putExtra("time", time);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        int currentMonth = month+1;
        tv_dateResult.setText(dayOfMonth + " / " + currentMonth + " / " + year);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        tv_timeResult.setText(hourOfDay + ":" + minute);
    }

    public void clickCancel(View view) {
        startActivity(new Intent(this, Homepage.class));
    }


    @Override
    public void onClick(View v) {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "Time Picker");
    }

}