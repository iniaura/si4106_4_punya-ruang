package com.example.tubespunyaruang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class homepage2 extends AppCompatActivity {
    TextView namaRuang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage2);

        namaRuang = findViewById(R.id.tv_nama_ruang);
    }

    public void clickPesan(View view) {
        Intent intent = new Intent(this, pemesanan.class);
        intent.putExtra("namaRuang", namaRuang.getText());
        startActivity(intent);
    }
}
