package com.example.tubespunyaruang;

public class pesanan {
    public String namaRuang, namaAcara, namaPenyelenggara, tanggal, waktu, key;

    public pesanan(){}

    public pesanan(String namaRuang, String namaAcara, String namaPenyelenggara, String tanggal, String waktu) {
        this.namaRuang = namaRuang;
        this.namaAcara = namaAcara;
        this.namaPenyelenggara = namaPenyelenggara;
        this.tanggal = tanggal;
        this.waktu = waktu;

    }

    public String getnamaRuang() {
        return namaRuang;
    }

    public void setnamaRuang(String namaRuang) {
        namaRuang = namaRuang;
    }

    public String getnamaAcara() {
        return namaAcara;
    }

    public void setnamaAcara(String namaAcara) {
        namaAcara = namaAcara;
    }

    public String getnamaPenyelenggara() {
        return namaPenyelenggara;
    }

    public void setnamaPenyelenggara(String namaPenyelenggara) { namaPenyelenggara = namaPenyelenggara; }

    public String gettanggal() {
        return tanggal;
    }

    public void settanggal(String tanggal) {
        tanggal = tanggal;
    }

    public String getwaktu() {
        return waktu;
    }

    public void setwaktu(String waktu) {
        waktu = waktu;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        key = key;
    }
}
