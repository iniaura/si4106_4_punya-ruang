package com.example.tubespunyaruang;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

class PesananAdapterRecyclerView extends RecyclerView.Adapter<PesananAdapterRecyclerView.MyViewHolder>{
    private List<pesanan> daftarPesanan;

    public PesananAdapterRecyclerView(ArrayList<pesanan> daftarPesanan) {
        this.daftarPesanan = daftarPesanan;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cardOrder;
        public TextView order_nama_ruang, order_tanggal_pesan,order_waktu_pesan;

        public MyViewHolder(View view) {
            super(view);
            cardOrder = view.findViewById(R.id.cardOrder);
            order_nama_ruang = view.findViewById(R.id.order_nama_ruang);
            order_tanggal_pesan = view.findViewById(R.id.order_tanggal_pesan);
            order_waktu_pesan = view.findViewById(R.id.order_waktu_pesan);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order, parent, false);

        return new MyViewHolder(itemView);
    }


//    @NonNull
//    @Override
//    public RequestAdapterRecyclerView.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return null;
//    }

    @Override
    public void onBindViewHolder(@NonNull PesananAdapterRecyclerView.MyViewHolder holder, int position) {
        final pesanan pesanan = daftarPesanan.get(position);

        holder.order_nama_ruang.setText(pesanan.getnamaRuang());
        holder.order_tanggal_pesan.setText(pesanan.gettanggal());
        holder.order_waktu_pesan.setText(pesanan.getwaktu());
    }

    @Override
    public int getItemCount() {
        return daftarPesanan.size();
    }
}
