package com.example.tubespunyaruang;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Set;

public class Homepage extends AppCompatActivity {
    Intent intent;
    String nama,email;
    int kodegoogle;
    TextView nama_ruang1, nama_ruang2;
    static final String TAG = "cobadlu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        nama_ruang1 = findViewById(R.id.nama_ruang1);
        nama_ruang2 = findViewById(R.id.nama_ruang2);

        setTitle("Homepage");
        intent = getIntent();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            nama = bundle.getString("namadisplay");
            email = bundle.getString("emaildisplay");
            kodegoogle = bundle.getInt("inilogingoogle");
            //kodegoogle = Integer.parseInt(bundle.getString("inilogingoogle"));
        }
        Log.i(TAG, "nama nya: "+nama+" dan emailnya : "+email);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
//respond to menu item selection
        switch (item.getItemId()) {
            case R.id.history_order:
                startActivity(new Intent(Homepage.this, HistoryOrder.class));
                return true;
            case R.id.profil:
                Intent intent = new Intent(Homepage.this,Profil.class);
                intent.putExtra("namadisplay",nama);
                intent.putExtra("emaildisplay",email);
                intent.putExtra("inilogingoogle",kodegoogle);
                Log.i(TAG, "onComplete: "+email);
                startActivity(intent);
//                startActivity( new Intent(Homepage.this, Profil.class));
                return true;
            case R.id.setting:
                startActivity(new Intent(Homepage.this, SettingsActivity.class));
                return true;

            default:
                // Do nothing
        }
        return super.onOptionsItemSelected(item);
    }


    public void inforuang(View view) {
        startActivity(new Intent(this, homepage2.class));
    }
}

